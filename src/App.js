import React from "react";
import SearchFilter from "./searchFilter/searchFilter";
import Form from "./form/Form";
import TableAg from "./table/TableAg";
import Charts from "./charts/Charts";
import QrScanner from "./qrcode/QrScanner";
import Select from "react-select";

function App() {
  const [toggle, setToggle] = React.useState(false);
  const [state, setState] = React.useState(0);
  const data = [
    { value: "charts", label: "Switch to Tables" },
    { value: "table", label: "Switch to Search and Filter" },
    { value: "search", label: " Switch to QR Generator" },
    { value: "qrscan", label: " Switch to Charts" },
  ];

  const dataSelect = [
    { value: "charts", label: "Charts" },
    { value: "table", label: "Table" },
    { value: "search", label: " Search and Filter" },
    { value: "qrscan", label: " QR Generator" },
  ];
  function MySelect(props) {
    const handleChange = (value) => {
      const abc = dataSelect.findIndex((e, idx) => e === value);
      setState(abc);
    };

    return (
      <div
        style={{
          width: "20vw",
          zIndex: 4,
          position: "absolute",
          left: "15%",
          top: "1%",
        }}
      >
        <Select
          id="color"
          options={dataSelect}
          onChange={handleChange}
          value={dataSelect[state]}
        />
      </div>
    );
  }

  const getData = () => {
    switch (data[state].value) {
      case "charts":
        return <Charts />;
        break;
      case "table":
        return <TableAg />;
        break;
      case "search":
        return <SearchFilter />;
        break;
      case "qrscan":
        return <QrScanner />;
      default:
        break;
    }
  };

  return (
    <div>
      <div
        style={{
          position: "absolute",
          top: "1%",
          right: "15%",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <button
          style={{
            background: "#345",
            width: "auto",
            height: "50px",
            cursor: "pointer",
            color: "#fff",
          }}
          onClick={() => setState(state >= data.length - 1 ? 0 : state + 1)}
        >
          <p style={{ color: "#fff", fontSize: "15px", margin: 0 }}>
            {data[state].label}
          </p>
        </button>
      </div>
      <MySelect />
      <div
        style={{
          margin: "0 auto",
          width: "100vw",
          height: "100vh",
          display: "flex",
          justifyContent: "center",
        }}
      >
        {getData()}
      </div>
    </div>
  );
}

export default App;
