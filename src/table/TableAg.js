import React, { useState } from "react";
import { AgGridColumn, AgGridReact } from "ag-grid-react";
import "ag-grid-enterprise";

import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";

const TableAg = () => {
  const [gridApi, setGridApi] = useState(null);
  const [gridColumnApi, setGridColumnApi] = useState(null);

  const [tableData, setTableData] = React.useState([]);
  const getData = async () => {
    await fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log(data);
        setTableData(data);
      });
  };

  React.useEffect(() => {
    getData();
  }, []);

  const [rowData, setRowData] = useState([]);
  console.log(rowData);
  React.useEffect(() => {
    fetch("https://www.ag-grid.com/example-assets/row-data.json")
      .then((result) => result.json())
      .then((rowData) => setRowData(rowData));
  }, []);

  const onButtonClick = (e) => {
    const selectedNodes = gridApi.getSelectedNodes();
    const selectedData = selectedNodes.map((node) => node.data);
    const selectedDataStringPresentation = selectedData
      .map((node) => node.make + " " + node.model)
      .join(", ");
    alert(`Selected nodes: ${selectedDataStringPresentation}`);
  };

  const BtnCellRenderer = (props) => {
    const btnClickedHandler = (data) => {
      console.log(data.target.value);
      console.log(props);
    };
    return <button onClick={btnClickedHandler}>Click Me!</button>;
  };

  const col = [
    { headerName: "Make", field: "make", checkboxSelection: true },
    { headerName: "Model", field: "model" },
    { headerName: "Price", field: "price" },
  ];

  return (
    <div
      style={{
        width: "80vw",
        height: "100vh",
        background: "#953",
        // alignItems: "center",
        justifyContent: "center",
        display: "flex",
      }}
    >
      <div className="ag-theme-alpine" style={{ height: 400, width: 600 }}>
        <h1
          style={{
            alignItems: "center",
            display: "flex",
            justifyContent: "center",
            color: "#fff",
          }}
        >
          Table
        </h1>
        <div style={{ justifyContent: "center", display: "flex" }}>
          <button
            onClick={onButtonClick}
            style={{ width: "15vw", height: "5vh", margin: 20 }}
          >
            Get selected rows
          </button>
        </div>
        <AgGridReact
          rowData={rowData}
          rowSelection="multiple"
          onGridReady={(params) => setGridApi(params.api)}
          pagination
          frameworkComponents={{
            btnCellRenderer: BtnCellRenderer,
          }}
          defaultColDef={{
            editable: true,
            sortable: true,
            flex: 1,
            minWidth: 100,
            filter: true,
            resizable: true,
          }}
          columnDefs={col}
        />
      </div>
    </div>
  );
};

export default TableAg;
