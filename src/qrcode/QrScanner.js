import React, { Suspense } from "react";
import QRCode from "qrcode-react";
import QRForm from "./QrForm";
import "../App.css";

export default function QrScanner() {
  const [text, setText] = React.useState("");
  const [data, setData] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  //   const LazyLoad = React.lazy(() => import("../charts/SimpleLineChart"));
  const dataM = {
    name: "hello",
    type: "pet",
    age: 23,
    url: "http://chawandinesh.github.io",
  };

  const handleSubmitStatus = (status) => {
    //  console.log(status, 'status')
    console.log(status, JSON.stringify(status))
    setData(JSON.stringify(status));
  };
  return (
    <div
      style={{
        background: "#c48f2b",
        width: "80vw",
        margin: "0 auto",
        minHeight: "100vh",
        height: "auto"
      }}
    >
      <h1
        style={{
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          color: "#fff",
        }}
      >
        QR Generator
      </h1>
      <div
        style={{
          width: "80vw",
          height: "90vh",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <div
          style={{
            width: " 50vw",
            background: "#fff",
            height: "auto",
            padding: "30px",
            display: "flex",
            alignContent: "center",
            justifyContent: "center",
          }}
        >
          {data ? (
              <div style={{display:'flex',flexDirection:'column', justifyContent:'center'}}>
            <QRCode
              value={data}
              size={128}
              bgColor={"#ffffff"}
              fgColor={"#000000"}
              level={"L"}
              includeMargin={false}
              renderAs={"svg"}
              imageSettings={{
                src: "https://static.zpao.com/favicon.png",
                x: null,
                y: null,
                height: 24,
                width: 24,
                excavate: true,
              }}
            />
            <button onClick={() => setData("")} style={{marginTop: '2vw', background:'#87f', color:'#fff', padding: 6}}>Generate Again</button>
            </div>
          ) : (
            <QRForm getSubmitStatus={handleSubmitStatus} />
          )}
        </div>
      </div>
    </div>
  );
}
