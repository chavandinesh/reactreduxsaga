import React, { Suspense } from "react";
import BarChart from "./BarChart";
import SimpleLineChart from "./SimpleLineChart";
import SimpleBarChart from "./SimpleBarChart";
import "../App.css";

export default function Charts() {
  return (
    <div
      style={{
        background: "#6c2e78",
        width: "80vw",
        margin: "0 auto",
        height: "100vh",
      }}
    >
      <h1
        style={{
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          color: "#fff",
        }}
      >
        Charts
      </h1>
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          height: "80vh",
          justifyContent: "center",
          width: "80vw",
          overflowY: "scroll",
        }}
      >
        <div
          style={{
            width: "auto",
            height: "auto",
            marginBottom: 20,
            background: "#fff",
            borderWidth: 2,
            margin: 5,
          }}
        >
          <h2 style={{ display: "flex", justifyContent: "center" }}>
            Simple Line Chart
          </h2>
          <SimpleLineChart />
        </div>
        <div
          style={{
            width: "auto",
            height: "auto",
            background: "#fff",
            borderWidth: 2,
            margin: 5,
          }}
        >
          <h2 style={{ display: "flex", justifyContent: "center" }}>
            Simple Bar Chart
          </h2>

          <SimpleBarChart />
        </div>
        <div
          style={{
            width: "auto",
            height: "auto",
            background: "#fff",
            borderWidth: 2,
            margin: 5,
          }}
        >
          <h2 style={{ display: "flex", justifyContent: "center" }}>
            Custom Bar Chart
          </h2>

          <BarChart />
        </div>
        {/* <div
          style={{
            width: "50%",
            height: "40vh",
            background: "#fff",
            borderWidth: 2,
            margin: 5,
          }}
        ></div> */}
      </div>
    </div>
  );
}
