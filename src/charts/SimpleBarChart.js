import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  CartesianGrid,
  Legend,
} from "recharts";

export default function SimpleBarChart() {
  const data = [
    { name: "Page A", uv: 400, pv: 2400, amt: 2400 },
    { name: "Page B", uv: 100, pv: 900, amt: 400 },
    { name: "Page A", uv: 789, pv: 2600, amt: 700 },
    { name: "Page B", uv: 560, pv: 2000, amt: 2400 },
    { name: "Page C", uv: 100, pv: 900, amt: 400 },
    { name: "Page A", uv: 789, pv: 2600, amt: 700 },
    { name: "Page C", uv: 560, pv: 2000, amt: 2400 },
  ];

  return (
    <BarChart width={600} height={300} data={data}>
      <XAxis dataKey="name" />
      <YAxis />
      <Bar dataKey="uv" barSize={30} fill="#8884d8" />
    </BarChart>
  );
}
