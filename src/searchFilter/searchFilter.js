import React, { Suspense } from "react";
import "../App.css";

export default function SearchFilter() {
  const [text, setText] = React.useState("");
  const [data, setData] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  //   const LazyLoad = React.lazy(() => import("../charts/SimpleLineChart"));

  const fetchData = async () => {
    await fetch("https://jsonplaceholder.typicode.com/todos")
      .then((res) => {
        return res.json();
      })
      .then((data) => setData(data.slice(0, 100)))

      .catch((err) => {
        console.log(err, "err");
      });
  };
  React.useEffect(() => {
    fetchData();
  }, []);

  React.useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }, [text]);
  return (
    <div
      style={{
        background: "#377",
        width: "80vw",
        margin: "0 auto",
        height: "100vh",
      }}
    >
      <h1
        style={{
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          color: "#fff",
        }}
      >
        Search and filter
      </h1>

      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <input type="text" placeholder="Enter here..." onChange={(e) => setText(e.target.value)} />
        {/* <button style={{ background: "#fff", color: "#236" }}>search</button> */}
        <p style={{ color: "#fff" }}>
          {" "}
          Showing {data.filter((e) => e.title.includes(text)).length} out of{" "}
          {data.length} items
        </p>
      </div>
      <div
        style={{
          width: "40vw",
          padding: 30,
          margin: "0 auto",
          marginTop: "10vh",
          height: "50vh",
          borderTopLeftRadius: 40,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          overflowY: "scroll",
          background: "#893",
        }}
      >
        {!loading ? (
          data.filter((e) => e.title.includes(text)).length ? (
            data
              .filter((e) => e.title.includes(text))
              .map((e, idx) => {
                return (
                  <p
                    style={{
                      color: "#fff",
                      backgroundColor: "rgba(0,0,0,0.4)",
                      width: "30vw",
                      padding: 20,
                    }}
                  >
                    {e.title}
                  </p>
                );
              })
          ) : (
            <h2 style={{ color: "#fff" }}>NO RESULTS</h2>
          )
        ) : (
          <div style={{ alignItems: "center", justifyContent: "center" }}>
            <div class="loader" style={{ margin: "0 auto" }}></div>
            <h1 style={{ color: "#ffd" }}>Loading...</h1>
          </div>
        )}
      </div>
    </div>
  );
}
